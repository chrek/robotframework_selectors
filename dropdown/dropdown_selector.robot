*** Settings ***
Documentation  Working with Dropdown, Checkbox, and Radio Buttons in Robot Framework
Library  SeleniumLibrary

*** Test Cases ***
Validate user can select items from the dropdown
    [documentation]  Test case to verify that user can select values from the dropdown
    [tags]  Smoke
    Open Browser  https://the-internet.herokuapp.com/dropdown  Chrome
    Wait Until Element Is Visible  id:dropdown  timeout=5
    Select From List By Index  id:dropdown  1
    List Selection Should Be  id:dropdown  Option 1
    Select From List By Value  id:dropdown  2
    List Selection Should Be  id:dropdown  Option 2
    Close Browser